import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostsDetailComponent } from './posts-detail/posts-detail.component';
import { PostsListComponent } from './posts-list/posts-list.component';

const routes: Routes = [
  {
    path: '',
    title: 'Posts',
    component: PostsListComponent,
  },
  {
    path: ':postId',
    title: 'Detailed Post',
    component: PostsDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsRoutingModule {}
