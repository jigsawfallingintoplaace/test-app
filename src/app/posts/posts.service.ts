import { DecimalPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Post } from '@shared/models/rest-data.model';
import { PageService } from '@shared/services/page.service';
import { RestService } from '@shared/services/rest.service';

@Injectable({
  providedIn: 'root',
})
export class PostsService extends PageService<Post> {
  constructor(
    router: Router,
    route: ActivatedRoute,
    pipe: DecimalPipe,
    private restApiService: RestService
  ) {
    super(router, route, pipe);
  }

  getData() {
    return this.restApiService.getPosts();
  }
}
