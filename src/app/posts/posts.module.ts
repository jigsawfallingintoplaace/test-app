import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { SharedModule } from '@shared/shared.module';

import { PostsRoutingModule } from './posts-routing.module';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostsDetailComponent } from './posts-detail/posts-detail.component';
import { PostsService } from './posts.service';

@NgModule({
  declarations: [PostsListComponent, PostsDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    PostsRoutingModule,
    SharedModule,
  ],
  providers: [PostsService, DecimalPipe],
})
export class PostsModule {}
