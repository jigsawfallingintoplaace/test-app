import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { Post } from '@shared/models/rest-data.model';
import { RestService } from '@shared/services/rest.service';

@Component({
  selector: 'app-posts-detail',
  templateUrl: './posts-detail.component.html',
  styleUrls: ['./posts-detail.component.scss'],
})
export class PostsDetailComponent implements OnInit {
  post$: Observable<Post>;

  constructor(
    private route: ActivatedRoute,
    private restApiService: RestService
  ) {}

  ngOnInit(): void {
    const params = this.route.snapshot.params;
    this.post$ = this.restApiService.getPost(params['postId']);
  }
}
