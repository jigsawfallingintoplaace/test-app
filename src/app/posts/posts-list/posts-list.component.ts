import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable, takeUntil } from 'rxjs';

import { Post } from '@shared/models/rest-data.model';
import {
  SortableDirective,
  SortEvent,
} from '@shared/directives/sortable.directive';
import { POSTS_COLUMNS } from '@shared/models/column-map.model';
import { Unsubscriber } from '@shared/services/unsubscriber';

import { PostsService } from '../posts.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
})
export class PostsListComponent extends Unsubscriber implements OnInit {
  posts$: Observable<Post[]>;
  columns = POSTS_COLUMNS;

  @ViewChildren(SortableDirective) headers: QueryList<SortableDirective>;

  constructor(public pageService: PostsService) {
    super();
    if (!this.pageService.searchColumn) {
      this.pageService.searchColumn = 'title';
    }
  }

  ngOnInit(): void {
    this.posts$ = this.pageService.entries$.pipe(
      takeUntil(this.endSubscriptions$)
    );
  }

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.appSortable !== column) {
        header.direction = '';
      }
    });

    this.pageService.sortColumn = column as keyof Post;
    this.pageService.sortDirection = direction;
  }
}
