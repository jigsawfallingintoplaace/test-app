import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { Album, Post, User } from '@shared/models/rest-data.model';
import { RestService } from '@shared/services/rest.service';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.scss'],
})
export class UsersDetailComponent implements OnInit {
  user$: Observable<User>;
  userAlbums$: Observable<Album[]>;
  userPosts$: Observable<Post[]>;

  constructor(
    private route: ActivatedRoute,
    private restApiService: RestService
  ) {}

  ngOnInit(): void {
    const params = this.route.snapshot.params;
    this.user$ = this.restApiService.getUser(params['userId']);
    this.userAlbums$ = this.restApiService.getUserAlbums(params['userId']);
    this.userPosts$ = this.restApiService.getUserPosts(params['userId']);
  }
}
