import { Component, OnInit } from '@angular/core';
import { Album, Photo, Post } from '@shared/models/rest-data.model';

import { RestService } from '@shared/services/rest.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  posts$: Observable<Post[]>;
  photos$: Observable<Photo[]>;
  albums$: Observable<Album[]>;

  constructor(private restApiService: RestService) {}

  ngOnInit(): void {
    this.posts$ = this.restApiService.getPosts();
    this.albums$ = this.restApiService.getAlbums();
    this.photos$ = this.restApiService.getPhotos();
  }
}
