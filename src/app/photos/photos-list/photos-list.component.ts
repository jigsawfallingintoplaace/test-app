import { Component, OnInit } from '@angular/core';
import { Observable, takeUntil } from 'rxjs';

import { Photo } from '@shared/models/rest-data.model';
import { PHOTOS_COLUMNS } from '@shared/models/column-map.model';
import { Unsubscriber } from '@shared/services/unsubscriber';

import { PhotosService } from '../photos.service';

@Component({
  selector: 'app-photos-list',
  templateUrl: './photos-list.component.html',
  styleUrls: ['./photos-list.component.scss'],
})
export class PhotosListComponent extends Unsubscriber implements OnInit {
  photos$: Observable<Photo[]>;
  columns = PHOTOS_COLUMNS;

  constructor(public pageService: PhotosService) {
    super();
    if (!this.pageService.searchColumn) {
      this.pageService.searchColumn = 'albumId';
    }
    this.pageService.pageSize = 18;
  }

  ngOnInit(): void {
    this.photos$ = this.pageService.entries$.pipe(
      takeUntil(this.endSubscriptions$)
    );
  }
}
