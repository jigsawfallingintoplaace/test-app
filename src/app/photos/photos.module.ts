import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { SharedModule } from '@shared/shared.module';

import { PhotosRoutingModule } from './photos-routing.module';
import { PhotosListComponent } from './photos-list/photos-list.component';
import { PhotosDetailComponent } from './photos-detail/photos-detail.component';
import { PhotosService } from './photos.service';

@NgModule({
  declarations: [PhotosListComponent, PhotosDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    PhotosRoutingModule,
    NgxPaginationModule,
  ],
  providers: [PhotosService],
})
export class PhotosModule {}
