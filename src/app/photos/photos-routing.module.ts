import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PhotosDetailComponent } from './photos-detail/photos-detail.component';
import { PhotosListComponent } from './photos-list/photos-list.component';

const routes: Routes = [
  {
    path: '',
    title: 'Photos',
    component: PhotosListComponent,
  },
  {
    path: ':photoId',
    title: 'Detailed Photo',
    component: PhotosDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhotosRoutingModule {}
