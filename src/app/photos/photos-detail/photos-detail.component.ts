import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { Photo } from '@shared/models/rest-data.model';
import { RestService } from '@shared/services/rest.service';

@Component({
  selector: 'app-photos-detail',
  templateUrl: './photos-detail.component.html',
  styleUrls: ['./photos-detail.component.scss'],
})
export class PhotosDetailComponent implements OnInit {
  photo$: Observable<Photo>;

  constructor(
    private route: ActivatedRoute,
    private restApiService: RestService
  ) {}

  ngOnInit(): void {
    const params = this.route.snapshot.params;
    this.photo$ = this.restApiService.getPhoto(params['photoId']);
  }
}
