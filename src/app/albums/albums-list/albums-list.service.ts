import { DecimalPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, map } from 'rxjs';

import { Album } from '@shared/models/rest-data.model';
import { PageService } from '@shared/services/page.service';
import { RestService } from '@shared/services/rest.service';

@Injectable({
  providedIn: 'root',
})
export class AlbumsListService extends PageService<Album> {
  constructor(
    router: Router,
    route: ActivatedRoute,
    pipe: DecimalPipe,
    private restApiService: RestService
  ) {
    super(router, route, pipe);
  }

  getData() {
    return forkJoin({
      albums: this.restApiService.getAlbums(),
      photos: this.restApiService.getPhotos(),
    }).pipe(
      map(({ albums, photos }) =>
        albums.map((album) => ({
          ...album,
          thumbnailUrls: photos
            .filter((photo) => photo.albumId === album.id)
            .map((photo) => photo.thumbnailUrl)
            .slice(0, 4),
        }))
      )
    );
  }
}
