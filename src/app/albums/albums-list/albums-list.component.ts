import { Component, OnInit } from '@angular/core';
import { Observable, takeUntil } from 'rxjs';

import { Album } from '@shared/models/rest-data.model';
import { ALBUMS_COLUMNS } from '@shared/models/column-map.model';
import { Unsubscriber } from '@shared/services/unsubscriber';

import { AlbumsListService } from './albums-list.service';

@Component({
  selector: 'app-albums-list',
  templateUrl: './albums-list.component.html',
  styleUrls: ['./albums-list.component.scss'],
  providers: [AlbumsListService],
})
export class AlbumsListComponent extends Unsubscriber implements OnInit {
  albums$: Observable<Album[]>;
  columns = ALBUMS_COLUMNS;

  constructor(public pageService: AlbumsListService) {
    super();
    if (!this.pageService.searchColumn) {
      this.pageService.searchColumn = 'title';
    }
    this.pageService.pageSize = 16;
  }

  ngOnInit(): void {
    this.albums$ = this.pageService.entries$.pipe(
      takeUntil(this.endSubscriptions$)
    );
  }
}
