import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AlbumsListService } from './albums-list.service';

describe('AlbumsService', () => {
  let service: AlbumsListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [DecimalPipe],
    });
    service = TestBed.inject(AlbumsListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
