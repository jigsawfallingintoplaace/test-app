import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AlbumsDetailService } from './albums-detail.service';

describe('AlbumsDetailService', () => {
  let service: AlbumsDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [DecimalPipe],
    });
    service = TestBed.inject(AlbumsDetailService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
