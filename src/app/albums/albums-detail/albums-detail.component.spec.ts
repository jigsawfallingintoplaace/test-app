import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AlbumsDetailComponent } from './albums-detail.component';

describe('AlbumsDetailComponent', () => {
  let component: AlbumsDetailComponent;
  let fixture: ComponentFixture<AlbumsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AlbumsDetailComponent],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [DecimalPipe],
    }).compileComponents();

    fixture = TestBed.createComponent(AlbumsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
