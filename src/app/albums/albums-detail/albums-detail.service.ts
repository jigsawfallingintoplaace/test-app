import { DecimalPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Photo } from '@shared/models/rest-data.model';
import { PageService } from '@shared/services/page.service';
import { RestService } from '@shared/services/rest.service';

@Injectable({
  providedIn: 'root',
})
export class AlbumsDetailService extends PageService<Photo> {
  params: Params;

  constructor(
    router: Router,
    route: ActivatedRoute,
    pipe: DecimalPipe,
    private restApiService: RestService
  ) {
    super(router, route, pipe);
    this.params = route.snapshot.params;
  }

  getData() {
    return this.restApiService.getAlbumPhotos(this.params['albumId']);
  }
}
