import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, takeUntil } from 'rxjs';

import { Album, Photo } from '@shared/models/rest-data.model';
import { RestService } from '@shared/services/rest.service';
import { Unsubscriber } from '@shared/services/unsubscriber';

import { AlbumsDetailService } from './albums-detail.service';

@Component({
  selector: 'app-albums-detail',
  templateUrl: './albums-detail.component.html',
  styleUrls: ['./albums-detail.component.scss'],
  providers: [AlbumsDetailService],
})
export class AlbumsDetailComponent extends Unsubscriber implements OnInit {
  album$: Observable<Album | null>;
  albumPhotos$: Observable<Photo[]>;

  constructor(
    private route: ActivatedRoute,
    private restApiService: RestService,
    public pageService: AlbumsDetailService
  ) {
    super();
    if (!this.pageService.searchColumn) {
      this.pageService.searchColumn = 'title';
    }
    this.pageService.pageSize = 18;
  }

  ngOnInit(): void {
    const params = this.route.snapshot.params;
    this.album$ = this.restApiService.getAlbum(params['albumId']);
    this.albumPhotos$ = this.pageService.entries$.pipe(
      takeUntil(this.endSubscriptions$)
    );
  }
}
