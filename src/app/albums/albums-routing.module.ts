import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlbumsDetailComponent } from './albums-detail/albums-detail.component';
import { AlbumsListComponent } from './albums-list/albums-list.component';

const routes: Routes = [
  {
    path: '',
    title: 'Albums',
    component: AlbumsListComponent,
  },
  {
    path: ':albumId',
    title: 'Detailed Album',
    component: AlbumsDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlbumsRoutingModule {}
