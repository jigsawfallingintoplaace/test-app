import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { SharedModule } from '@shared/shared.module';

import { AlbumsRoutingModule } from './albums-routing.module';
import { AlbumsListComponent } from './albums-list/albums-list.component';
import { AlbumsDetailComponent } from './albums-detail/albums-detail.component';
import { AlbumsListService } from './albums-list/albums-list.service';

@NgModule({
  declarations: [AlbumsListComponent, AlbumsDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    AlbumsRoutingModule,
    SharedModule,
    NgxPaginationModule,
  ],
  providers: [AlbumsListService, AlbumsDetailComponent, DecimalPipe],
})
export class AlbumsModule {}
