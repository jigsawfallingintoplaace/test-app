import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Album, Photo, Post, User } from '../models/rest-data.model';

@Injectable({
  providedIn: 'root',
})
export class RestService {
  // can be moved to .env if needed
  private readonly restApiUrl = 'https://jsonplaceholder.typicode.com';

  constructor(private http: HttpClient) {}

  getPosts(request?: Partial<Post>): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.restApiUrl}/posts`, {
      params: { ...request },
    });
  }

  getPost(postId: number): Observable<Post> {
    return this.http.get<Post>(`${this.restApiUrl}/posts/${postId}`);
  }

  getAlbums(request?: Partial<Album>): Observable<Album[]> {
    return this.http.get<Album[]>(`${this.restApiUrl}/albums`, {
      params: { ...request },
    });
  }

  getAlbumPhotos(albumId: number): Observable<Photo[]> {
    return this.http.get<Photo[]>(
      `${this.restApiUrl}/albums/${albumId}/photos`
    );
  }

  getAlbum(albumId: number): Observable<Album> {
    return this.http.get<Album>(`${this.restApiUrl}/albums/${albumId}`);
  }

  getPhotos(request?: Partial<Photo>): Observable<Photo[]> {
    return this.http.get<Photo[]>(`${this.restApiUrl}/photos`, {
      params: { ...request },
    });
  }

  getPhoto(photoId: number): Observable<Photo> {
    return this.http.get<Photo>(`${this.restApiUrl}/photos/${photoId}`);
  }

  getUser(userId: number): Observable<User> {
    return this.http.get<User>(`${this.restApiUrl}/users/${userId}`);
  }

  getUserPosts(userId: number): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.restApiUrl}/users/${userId}/posts`);
  }

  getUserAlbums(userId: number): Observable<Album[]> {
    return this.http.get<Album[]>(`${this.restApiUrl}/users/${userId}/albums`);
  }
}
