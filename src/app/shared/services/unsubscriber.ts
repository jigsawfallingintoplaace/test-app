import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  template: '',
})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export class Unsubscriber implements OnDestroy {
  endSubscriptions$ = new Subject();

  ngOnDestroy(): void {
    this.endSubscriptions$.next(undefined);
    this.endSubscriptions$.unsubscribe();
  }
}
