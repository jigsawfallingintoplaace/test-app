import { PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, take, tap } from 'rxjs/operators';

import { SortDirection } from '@shared/directives/sortable.directive';

interface State<T> {
  page: number;
  pageSize: number;
  searchTerm: string;
  searchColumn: string;
  sortColumn: keyof T | '';
  sortDirection: SortDirection;
}

const compare = <T>(v1: T, v2: T) => (v1 < v2 ? -1 : v1 > v2 ? 1 : 0);

function sort<T>(entries: T[], column: keyof T | '', direction: string): T[] {
  if (direction === '' || column === '') {
    return entries;
  } else {
    return [...entries].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches<T>(searchFrom: T, searchTerm: string, pipe: PipeTransform) {
  switch (typeof searchFrom) {
    case 'string':
      return searchFrom.toLowerCase().includes(searchTerm.toLowerCase());
    case 'number':
      return pipe.transform(searchFrom).includes(searchTerm);
    default:
      true;
  }
}

export abstract class PageService<T> {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _cache: T[];
  private _entries$ = new BehaviorSubject<T[]>([]);

  private _state: State<T> = {
    page: 1,
    pageSize: 5,
    searchTerm: '',
    searchColumn: '',
    sortColumn: '',
    sortDirection: '',
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pipe: DecimalPipe
  ) {
    this._getStateFromQueryParams();

    // debounces input event triggers, if there is  cache - returns it,  otherwise requests data
    this._search$
      .pipe(
        tap(() => this._loading$.next(true)),
        debounceTime(200),
        switchMap(() => (this._cache ? of(this._cache) : this.getData())),
        tap((data) => {
          if (!this._cache) {
            this._cache = data;
          }
        }),
        switchMap((data) => this._search(data)),
        delay(200),
        tap(() => this._loading$.next(false))
      )
      .subscribe((posts) => {
        this._entries$.next(posts);
      });

    this._search$.next();
  }

  get entries$() {
    return this._entries$.asObservable();
  }
  get loading$() {
    return this._loading$.asObservable();
  }
  get page() {
    return this._state.page;
  }
  get pageSize() {
    return this._state.pageSize;
  }
  get searchTerm() {
    return this._state.searchTerm;
  }
  get searchColumn() {
    return this._state.searchColumn;
  }

  set page(page: number) {
    this._set({ page });
  }
  set pageSize(pageSize: number) {
    this._set({ pageSize });
  }
  set searchTerm(searchTerm: string) {
    this._set({ searchTerm });
  }
  set searchColumn(searchColumn: string) {
    this._set({ searchColumn });
  }
  set sortColumn(sortColumn: keyof T) {
    this._set({ sortColumn });
  }
  set sortDirection(sortDirection: SortDirection) {
    this._set({ sortDirection });
  }

  private _set(patch: Partial<State<T>>) {
    Object.assign(this._state, patch);
    this._search$.next();

    // updates active route with new query params
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: patch,
      queryParamsHandling: 'merge',
    });
  }

  private _search(searchFrom: T[]): Observable<T[]> {
    const { sortColumn, sortDirection, searchTerm, searchColumn } = this._state;

    // sort
    let entries = sort(searchFrom, sortColumn, sortDirection);

    // filter
    if (searchColumn && searchTerm?.trim()) {
      entries = entries.filter((entry) =>
        matches(entry[searchColumn as keyof T], searchTerm, this.pipe)
      );
    }
    return of(entries);
  }

  private _getStateFromQueryParams(): void {
    const params = this.route.snapshot.queryParams;
    Object.entries(params).forEach(([key, value]) => {
      if (Object.keys(this._state).includes(key) && value) {
        this._set({ [key]: value });
      }
    });
  }

  abstract getData(): Observable<T[]>;
}
