import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { SortableDirective } from './directives/sortable.directive';

@NgModule({
  declarations: [SortableDirective],
  exports: [SortableDirective],
  imports: [CommonModule, HttpClientModule],
  providers: [DecimalPipe],
})
export class SharedModule {}
