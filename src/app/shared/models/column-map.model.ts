import { Album, Photo, Post } from './rest-data.model';

interface Column<T> {
  value: keyof T;
  title: string;
  size?: number;
}

export const POSTS_COLUMNS: Column<Post>[] = [
  {
    value: 'id',
    title: 'ID',
    size: 1,
  },
  {
    value: 'userId',
    title: 'Author',
    size: 1,
  },
  {
    value: 'title',
    title: 'Title',
    size: 3,
  },
  {
    value: 'body',
    title: 'Content',
    size: 7,
  },
];

export const PHOTOS_COLUMNS: Column<Photo>[] = [
  {
    value: 'id',
    title: 'ID',
  },
  {
    value: 'albumId',
    title: 'Album ID',
  },
  {
    value: 'title',
    title: 'Title',
  },
];

export const ALBUMS_COLUMNS: Column<Album>[] = [
  {
    value: 'id',
    title: 'ID',
  },
  {
    value: 'userId',
    title: 'Author ID',
  },
];
